package ie.ucd.people;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.items.NotAlcoholicDrink;


/**
 * This class as a extend class of Person,
 * represents a person which can drink alcoholic drink.
 * And by counting the number of alcoholic drink 
 * to judge the person drunk or not
 * 
 * @author Wenxi Zhang 12257189
 */
public class Drinker extends Person{
	

	/**
	 * Declare all the parameter used in this class
	 */
	private double weight;
	private int numberOfDrinks;
	
	
	/**
	 * The constructor initializes the 
	 * weight and number of drinks
	 */
	public Drinker(){
		weight = 0;
		numberOfDrinks = 0;		
	}
	
	/**
	 * Get the weight of the person
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	/**
	 * Return the weight of the person
	 * @return the weight of the person
	 */	
	public double getWeight(){
		return weight;}
	
	/**
	 * Returns true if the drink is alcoholic or
	 * not alcoholic drink, otherwise return false.
	 * Also for every alcoholic drink the person take,
	 * number of drinks add 1.
	 * @return whether the drink is alcoholic or not
	 */
	public boolean drink(Drink beverage) {
		// TODO Auto-generated method stub
		if(beverage instanceof AlcoholicDrink){
			numberOfDrinks = numberOfDrinks + 1;
			return true	;
		}
		else 	
			if (beverage instanceof NotAlcoholicDrink){
		        return true;		
			}
			else 
				return false;		
	}
	
	/**
	 * Returns true if the person is drunk
	 * @return whether the person is drunk or not
	 */
	public boolean isDrunk(){		
		if(numberOfDrinks >= weight/10){
			return true;
				}
		return false;				
		}
		
	/**
	 * Auto-generated the abstract method of person
	 */
	public boolean eat(Food arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
