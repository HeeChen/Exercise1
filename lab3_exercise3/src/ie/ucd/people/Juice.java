package ie.ucd.people;

import ie.ucd.items.NotAlcoholicDrink;

/**
 * This class as a extend class of NotAlcoholicDrink,
 * represents a type of none-alcoholic drink.
 * So that the NotAlcoholicDrink can be used in main function.
 *  
 * @author Wenxi Zhang 12257189
 */
public class Juice extends NotAlcoholicDrink{
	
	/**
	 * Declare all the parameter used in this class
	 */
	private String name;
	private double volume;

	public Juice(String name, double volume) {
		super(name, volume);
		// TODO Auto-generated constructor stub
	}

}
