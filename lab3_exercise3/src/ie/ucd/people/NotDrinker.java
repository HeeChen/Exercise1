package ie.ucd.people;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.items.NotAlcoholicDrink;


/**
 * This class as a extend class of Person,
 * represents a person which can not drink alcoholic drink.
 * 
 * @author Wenxi Zhang 12257189
 */
public class NotDrinker extends Person{
	
	/**
	 * The constructor initializer
	 */	
	public NotDrinker(){}
		
	/**
	 * As the not drinker can only drink not alcoholic drinks,
	 * returns true if the drink is not alcoholic.
	 * Otherwise return false
	 * @return whether the drink is alcoholic or not.
	 */
	public boolean drink(Drink beverage) {
		// TODO Auto-generated method stub
		
		if(beverage instanceof AlcoholicDrink){
			return false;		
		}
		else 		
			if (beverage instanceof NotAlcoholicDrink){
		return true;
			}
			else 
				return false;
	}

	/**
	 * Auto-generated the abstract method of person
	 */
	public boolean eat(Food arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	
	//public abstract boolean drink(Drink drink)

}
