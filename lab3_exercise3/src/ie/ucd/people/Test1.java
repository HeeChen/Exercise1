package ie.ucd.people;

import ie.ucd.items.Drink;
import ie.ucd.items.Wine;
import ie.ucd.items.WineType;

/**
 * This class as a main class,
 * is used to test the other classes.
 *  
 * @author Wenxi Zhang 12257189
 */
public class Test1{

	public static void main(String[]args){
		
		/**
		 * Declare all the parameters. 
		 * Set the drink details and
		 * the weight of drinker.
		 */
		Person amy = new Drinker();	
		Person bob = new NotDrinker();
		Drink wine = new Wine ("Gunniess", 100, 10, WineType.Rose);
		Drink juice = new Juice("Orange", 100);
		amy.setWeight(45.8);
		
		/**
		 * By let each person drink, to test and out print the
		 * situation of each person.
		 */
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (bob.drink(wine))
		System.out.println("Bob drink alcoholic drink, he is a drinker.");
		else
		System.out.println("Bob cannot drink alcoholic drink, he is not a drinker.")	;
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (((Drinker) amy).isDrunk())
		System.out.println("Amy is drunk.");
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (((Drinker) amy).isDrunk())
		System.out.println("Amy is drunk.");
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (amy.drink(wine))
		System.out.println("Amy drink alcoholic drink, she is a drinker.");
		if (((Drinker) amy).isDrunk())
		System.out.println("Amy is drunk.");
					
		
	}
}
